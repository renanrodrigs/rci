## DESCRIÇÃO DO PROJETO RCI(Reciclagem Coletiva Inteligente)

A ideia se baseia em uma plataforma onde empresas que usam materiais recicláveis ou materiais específicos para descarte cadastrar suas localizações e quais tipos de materiais(em base um perfil do lugar) utilizam para conscientizar a comunidade a fazer um "descarte" ideal  de seus lixos contribuindo com a reciclagem na cidade.
Existirá um tipo de cadastro de empresa para receber os materiais e de pessoas que queiram descartar.As empresas também poderão mostrar seus trabalhos feitos com a reciclagem dos materiais que recebem como uma espécie de galeria ou feed.



## Visão RCI

#Para# comunidades em geral
#Cuja# falta de conhecimento de centros de tratação de lixos reciclaveis e vice-versa seja oculta
#a# RCI(Reciclagem Coletiva Inteligente)
#é# uma plataforma web adequada a troca de informações como uma rede social.
#que# as comunidades e empresas responsáveis por esse serviço,
possam ter um conhecimento para realizar a tratativa  de seu lixo da maneira correta
#diferentemente# de qualquer sistema municipal/estadual
#o# nosso produto garante uma solução simples e prática para fazer a ponte entre empresas para a tratativa de lixo reciclável e nossas comunidades.

##Personas
Empresas que tratam lixos específicos e/ou recicláveis, e comunidades em geral


#É# - Uma plataforma para armazenamento e troca de informações entre comunidades(cidadãos) e empresas(Tratadoras de lixo reciclável)

#Não é# - Uma plataforma para contrato de caçambas.

#Faz# - Uma ponte para conscientizar e mostrar as comunidades os pontos que podem tratar seus lixos e para as empresa o ponto de foco deles

#Não faz# - Comunicação para descarte de lixos orgânicos ou não específicos.

